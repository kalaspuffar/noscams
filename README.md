NoScams.tube source
===================

This is the source code for NoScams.tube. It's a plain old HTML site, served using GitLab Pages.

## Join the list?
If you want to join the list, see [issue 1](https://gitlab.com/VKCsh/noscams/-/issues/1).

---

## GitLab CI
This project's static Pages are built by GitLab CI, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - main
```

The above example expects to put all your HTML files in the `public/` directory.

---

## Why GitLab?
Because it's what I know! Someday I might self host this but GitLab is easy to set up for a project this size. I wanted it done quickly and GitLab made it quicker for me. :)
